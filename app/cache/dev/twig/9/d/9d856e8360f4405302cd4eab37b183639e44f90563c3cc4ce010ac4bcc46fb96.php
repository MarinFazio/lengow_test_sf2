<?php

/* TestBundle:Default:getoutput.html.twig */
class __TwigTemplate_9d856e8360f4405302cd4eab37b183639e44f90563c3cc4ce010ac4bcc46fb96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, (isset($context["output"]) ? $context["output"] : $this->getContext($context, "output")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "TestBundle:Default:getoutput.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
