<?php

/* TestBundle:Default:getjson.html.twig */
class __TwigTemplate_75443903b7aa0422732849aa0b6d010df71b86d22442cce4c8906561ddb6718a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, (isset($context["output"]) ? $context["output"] : $this->getContext($context, "output")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "TestBundle:Default:getjson.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
